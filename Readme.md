Run the project using a live server, such as Live Server or any other local server of your choice.

To use the templates:
- View the templates on the style guide by opening the style-guide.html file on localhost.
- Duplicate the index.css file and integrate it into your project.
- Copy the desired template from the style guide and implement it in your project.

Note: An example of how to use the templates can be found in the index.html file.